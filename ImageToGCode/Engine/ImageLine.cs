﻿using ImageToGCode.Engine.Geometry;
using ImageToGCode.Engine.Interpolators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ImageToGCode.Engine
{
    class ImageLine : Line
    {
        private Image _Image;

        private List<Pixel> _Pixels;
        public List<Pixel> Pixels
        {
            get
            {
                return _Pixels;
            }
        }

        //법선 벡터와 점을 사용한 직선 생성자
        public ImageLine(Vector normalVector, Vector pointToCross, Image image)
            :base(normalVector, pointToCross)
        {
            _Pixels = new List<Pixel>();
            _Image = image;
        }



        //법선 벡터를 통한 직선 생성자. 그리고 선이 통과하는 점의 좌표는 법선 벡터의 좌표와 일치합니다.
        public ImageLine(Vector normalVector, Image image)
            : this(normalVector, normalVector, image)
        {   }

        public void GeneratePixels(double pointResolution)
        {
            var directionVector = new Vector(-B, A).Normalize() * pointResolution; // _NormalVector.Normalize().Rotate90CCW() * pointResolution; 
            if (directionVector.X < 0)
                directionVector = directionVector.Reverse();

            var currentVector = GetFirstVector();
            
            IInterpolator inter = InterpolateHelper.CurrentInterpolator;

            // 픽셀로 채우기
            Pixel temp = _Image.GetPixel(currentVector.X, currentVector.Y);//inter.GetPixel(_Image, currentVector);
            while (temp != null)
            {
                _Pixels.Add(temp);
                currentVector += directionVector;
                temp = _Image.GetPixel(currentVector.X, currentVector.Y);//inter.GetPixel(_Image, currentVector);
            }

            var lastAddedVector = currentVector - directionVector;
            var lastVector = GetLastVector();
            if (lastAddedVector.X != lastVector.X && lastAddedVector.Y != lastVector.Y)
            {
                _Pixels.Add(_Image.GetPixel(lastVector.X, lastVector.Y));

            }
            

        }

        private Vector GetLastVector()
        {
            if (B == 0)
                return new Vector(-C / B, _Image.Height);

            double x = _Image.Width;
            double y = GetY(x);


            if (y > _Image.Height)
            {
                y = _Image.Height;
                x = GetX(y);
            }
            else if (y < 0)
            {
                y = 0;
                x = GetX(y);
            }

            return new Vector(x, y);
        }

        private Vector GetFirstVector() // 나중에 바꾸셔도 돼요...
        {
            if(B == 0)
                return new Vector(-C / A, 0);
            
            double x = 0.0;
            double y = GetY(x);


                if (y > _Image.Height)
                {
                    y = _Image.Height;
                    x = GetX(y);
                }
                else if (y < 0)
                {
                    y = 0;
                    x = GetX(y);
                }

            return new Vector(x, y);



            /*double temp;

            //교차로 c OX, y = 0
            temp = -C / A;
            if (0 <= temp && temp <= _Image.Width - 1)
                return new Vector(temp, 0);

            //교차로 с OY, x = 0
            temp = -C / B;
            if (0 <= temp && temp <= _Image.Height - 1)
                return new Vector(0, temp);

            //교차로 с y=max
            temp = (-C - B * (_Image.Height - 1)) / A;
            if (0 <= temp && temp <= _Image.Width - 1)
                return new Vector(temp, _Image.Height - 1);

            //교차로 с x=max
            temp = (-C - A * (_Image.Width - 1)) / B;
            if (0 <= temp && temp <= _Image.Height - 1)
                return new Vector(_Image.Width - 1, temp);

            throw new Exception("Line do not cross image");*/

        }
    }
}
