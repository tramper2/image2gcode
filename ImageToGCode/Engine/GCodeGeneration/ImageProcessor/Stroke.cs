﻿using ImageToGCode.Engine.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ImageToGCode.Engine.GCodeGeneration.ImageProcessor
{
    class Stroke : FreeMotionStroke
    {
        public Vector DestinationPoint { get; private set; }
        public double Intensity { get; private set; } // 1-검은색 0-흰색

        public Stroke(Vector destinationPoint, double intensity)
            : base(destinationPoint)
        {
            Intensity = intensity;
        }
    }
}
