﻿using ImageToGCode.Engine.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ImageToGCode.Engine.GCodeGeneration.ImageProcessor
{
    class IdleStroke : Stroke
    {
        //closestPoint - 선에서 가장 가까운 점 
        //pointForDirection - 가속선의 어떤 지점이라도
        public IdleStroke(Vector destination)
            : base(destination, 0)
        {

        }
    }
}
