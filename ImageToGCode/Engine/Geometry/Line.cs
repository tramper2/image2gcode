﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ImageToGCode.Engine.Geometry
{
    class Line
    {
        //똑바로 Ax+By+C=0 //직선인가?
        protected double A;
        protected double B;
        protected double C;


        public double GetX(double Y)
        {
            return (-C - B * Y) / A;
        }

        public double GetY(double X)
        {
            return (-C - A * X) / B;
        }


        //        법선 벡터와 점을 사용한 직선 생성자
        public Line (Vector normalVector, Vector pointToCross)
        {
            A = normalVector.X;
            B = normalVector.Y;

            C = -(A * pointToCross.X + B * pointToCross.Y);
        }

        //        법선 벡터를 통한 직선 생성자.그리고 점의 좌표는 법선 벡터의 좌표와 일치합니다.
        public Line(Vector normalVector) : this(normalVector, normalVector)
        { }

        public static Vector GetIntersection(Line l1, Line l2)
        {
            return new Vector(-(l1.C * l2.B - l2.C * l1.B) / (l1.A * l2.B - l2.A * l1.B), -(l1.A * l2.C - l2.A * l1.C) / (l1.A * l2.B - l2.A * l1.B));
        }
    }
}
