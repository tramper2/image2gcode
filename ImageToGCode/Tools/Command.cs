﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.Windows.Input;

namespace ImageToGCode.Tools
{
    class Command : ICommand
    {
        private SynchronizationContext _synchronizationContext = SynchronizationContext.Current;

        readonly Action<object> _executeAction;
        readonly Predicate<object> _canExecute;

        /// <summary>
        /// 명령 초기화
        /// </summary>
        /// <param name="executeAction">수행할 작업</param>
        /// <param name="canExecute"> 계산 능력을 계산하는 기능</param>
        public Command(Action<object> executeAction, Predicate<object> canExecute)
        {
            if (executeAction == null) throw new ArgumentNullException("executeAction", "executeAction is null.");
            if (canExecute == null) throw new ArgumentNullException("canExecute", "canExecute is null.");

            _executeAction = executeAction;
            _canExecute = canExecute;
        }
        public virtual bool CanExecute(object parameter)
        {
            return _canExecute(parameter);
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            if (!CanExecute(parameter))
                throw new Exception("명령을 사용할 수 없습니다.");
            _executeAction(parameter);
        }

        /// <summary>
        /// 시행 가능성 변경 알림 트리거
        /// </summary>
        public void RaiseCanExecuteChanged()
        {
            if (SynchronizationContext.Current != _synchronizationContext)
                RaiseCanExecuteChangedSynced(null);
            else
                _synchronizationContext.Post(RaiseCanExecuteChangedSynced, null);
        }
        private void RaiseCanExecuteChangedSynced(object param)
        {
            if (CanExecuteChanged != null)
                CanExecuteChanged(this, new EventArgs());
        }

    }

}
