﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;


namespace ImageToGCode
{
    class GCodeGenerator
    {
        private readonly double _Width;
        private readonly double _Height;
        private readonly double _LineStep;
        private readonly double _PointStep = 0.1;
        private readonly double _Freezone;
        private readonly double _Feed;
        private readonly bool _EngraveBothDirection;

        private const int MAX_Intencity = 80;

        public GCodeGenerator(double width, double height, double lineStep, double freezone, double feed, bool engraveBothDirection)
        {
            _Width = width;
            _Height = height;
            _LineStep = lineStep;
            _Freezone = freezone;
            _Feed = feed;
            _EngraveBothDirection = engraveBothDirection;
        }
        const string feedMove = "G1 X{0:0.###} S{1:0.###}";
        //const string engraveMove = "G1 X{0:0.###} S{1:0.###}";
        //const string freeMove = "G1 X{0:0.###} M5";
        const string fastMove = "G0 X{0:0.###} Y{1:0.###} S{2:0.###}";
        public IEnumerable<string> Generate(Bitmap image)
        {
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            
            int iWidth = image.Width;   //이미지 너비
            int iHeight = image.Height; //이미지 너비
            double pixelWidth = _Width / iWidth;    //픽셀 너비
            if (pixelWidth < _PointStep)
                pixelWidth = _PointStep;

            int xPixelSteps = 1;    //이미지의 픽셀 샘플링 단계
            if (pixelWidth < _LineStep)
                xPixelSteps = (int)(_LineStep / pixelWidth);

            int linesCount = (int)(_Height / _LineStep);
            double linesPerPixel = (double)linesCount / (double)iHeight;

            List<string> gCode = new List<string>();
            gCode.Add(string.Format(nfi, "F{0:0.###}", _Feed));
            for (int y = 0; y < linesCount; y++)    //라인을 따라 걷기
            {
                int yPixelCursor = (int)(y / linesPerPixel);    //사진 속 위치를 계산해 보세요

                int lastLaserState = 0;     //마지막 단계에서 레이저 밝기 재설정
                double lastX = 0.0;         //마지막 위치

                int firstDarkX = -1;        //첫 번째 어두운 픽셀 찾기
                for (int x = 0; x < iWidth; x++)
                {
                    if (image.GetPixel(x, yPixelCursor).GetBrightness() < 1.0)
                    {
                        firstDarkX = x;
                        break;
                    }
                }
                if (firstDarkX == -1)
                    continue;   //흰색인 경우 줄 건너뛰기


                //어두운 픽셀이 있는 줄의 시작 부분까지 유휴 상태입니다
                gCode.Add(string.Format(nfi, fastMove, firstDarkX * pixelWidth - _Freezone, -y * _LineStep, 0.0));

                //작동 속도로 첫 번째 암점에 접근합니다.
                //gCode.Add(string.Format(nfi, feedMove, firstDarkX * pixelWidth, 0));

                float brightnessSum = 0;
                //연속된 픽셀을 통해 반복
                for (int x = firstDarkX; x < iWidth; x++)
                {
                    if (x % xPixelSteps != 0)
                    {
                        brightnessSum += image.GetPixel(x, yPixelCursor).GetBrightness();   //머리 단계 사이의 픽셀 밝기를 합산합니다.
                        continue;
                    }

                    brightnessSum += image.GetPixel(x, yPixelCursor).GetBrightness(); //현재 픽셀 추가

                    int curLaserState = (int)(MAX_Intencity * (1.0 - brightnessSum / (double)xPixelSteps)); //평균 밝기를 계산하다
                    brightnessSum = 0;  //밝기 재설정

                    if (lastLaserState == curLaserState) //줄이 채워지지 않은 경우 명령을 추가하지 마십시오
                        continue;

                    lastX = x * pixelWidth; //색상 변화가 있는 점의 좌표

                    gCode.Add(string.Format(nfi, feedMove, lastX, lastLaserState));

                    lastLaserState = curLaserState;
                }

                if (lastLaserState > 0.0)
                {
                    lastX = _Width;
                    gCode.Add(string.Format(nfi, feedMove, _Width, lastLaserState)); //라인을 완성하고 레이저를 끕니다.
                }
                
                if (_Freezone > 0.0)
                    gCode.Add(string.Format(nfi, feedMove, lastX + _Freezone, 0)); //제동을 위한 공회전

            }
            return gCode;
        }
    }
}
